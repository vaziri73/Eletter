<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {
	public function __construct() {
        parent::__construct();
         $this->load->library('session');
         //$loged=$this->session->userdata();
         //print_r($loged);
         if ($this->session->userdata('logged_in')=='True') {
         	$this->load->helper(
        	 array('form','url')

        	);
        	$this->load->model('lettermodel');
         }
         else {
				//redirect('Login');	
         	header("Location: Login");
die();
	         }
	     }
public function setting()
{
	$setting['admin']=0;
	$setting['log']=null;
	$setting['users']=null;
	$user['user']=$this->session->userdata('u1');
	$user['role']='admin';
	$data['title']="تنظیمات";
	$data['user']=$this->session->userdata('name');
	$permission=$this->lettermodel->getpermission($user);
	if ($permission!=null) {
		$setting['admin']=1;
		$setting['log']=$this->lettermodel->getlog();
		$setting['users']=$this->lettermodel->getuserlist();
	}

	$this->load->view('header',$data);
	$this->load->view('setting',$setting);
	$this->load->view('footer');
}
public function delete(){
	$user = $this->input->post();
	$this->lettermodel->deleteuser($user);
	redirect('/Main');
}
public function updatepass(){
	$data['pass'] = $this->input->post('password');
	$data['user']=$this->session->userdata('u1');

	$this->lettermodel->setpass($data);
	redirect('/Main');
}

public function logout()
{
    $user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
    $this->session->sess_destroy();
    redirect('login');
}

}
?>